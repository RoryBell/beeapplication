﻿
namespace BeeLibrary
{
    public class DroneBee : Bee
    {
        public override bool IsDead
        {
            get
            {
                return (m_health >= 50f) ? false : true;
            }
        }

        public override string ToString()
        {
            return "Drone";
        }
    }
}
