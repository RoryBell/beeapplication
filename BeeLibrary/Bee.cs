﻿
namespace BeeLibrary
{
    /// <summary>
    /// Abstract class for the bee class.
    /// </summary>
    public abstract class Bee
    {
        /// <summary>
        /// Reduces the bee's health by the damage.
        /// </summary>
        /// <param name="damage">The amount to reduce the health by.</param>
        public void Damage(double damage)
        {
            // Make sure the bee is still alive to take damage.
            if (!IsDead)
            {
                // Reduce the health by the damage.
                m_health -= damage;

                // Set the health to 0 if it goes below.
                if (m_health < 0f)
                {
                    m_health = 0f;
                }
            }
        }

        /// <summary>
        /// Returns the death status of the bee.
        /// </summary>
        public abstract bool IsDead { get; }

        /// <summary>
        /// Returns the health of the bee.
        /// </summary>
        public double Health { get { return m_health; } }

        /// <summary>
        /// The type of bee.
        /// </summary>
        public string Type { get { return ToString(); } }

        /// <summary>
        /// Returns the status of the bee as a string.
        /// </summary>
        public string Status { get { return IsDead == true ? "Dead" : "Alive"; } }
        
        /// <summary>
        /// The health of the bee in percentage.
        /// </summary>
        protected double m_health = 100;
    }
}
