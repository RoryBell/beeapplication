﻿
namespace BeeLibrary
{
    public class QueenBee : Bee
    {
        public override bool IsDead
        {
            get
            {
                return (m_health >= 20f) ? false : true;
            }
        }

        public override string ToString()
        {
            return "Queen";
        }
    }
}
