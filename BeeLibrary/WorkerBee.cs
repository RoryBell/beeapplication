﻿
namespace BeeLibrary
{
    public class WorkerBee : Bee
    {
        public override bool IsDead
        {
            get
            {
                return (m_health >= 70f) ? false : true;
            }
        }

        public override string ToString()
        {
            return "Worker";
        }
    }
}
