﻿
namespace BeeApplication
{
    public struct ListItem
    {
        public string Type { get; }

        public bool IsDead { get; }

        public bool Health { get; }
    }
}
