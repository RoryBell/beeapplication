﻿using BeeLibrary;
using System;
using System.Windows;

namespace BeeApplication
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            for (int i = 0; i < 10; i++)
            {
                WorkerBee worker = new WorkerBee();
                QueenBee queen = new QueenBee();
                DroneBee drone = new DroneBee();

                AddBee(worker);
                AddBee(queen);
                AddBee(drone);
            }
        }

        /// <summary>
        /// Add the bee to the table as a new row.
        /// </summary>
        /// <param name="bee">The bee to add to the table.</param>
        private void AddBee(Bee bee)
        {
            BeeGrid.Items.Add(bee);
        }

        /// <summary>
        /// Applies the damage to a bee on the selected row.
        /// </summary>
        void DamageBee(object sender, RoutedEventArgs e)
        {
            // Get the item from the selected row.
            Bee bee = (Bee)BeeGrid.SelectedItem;

            // Randomly generate the damage and apply it to the selected bee.
            Random rand = new Random();
            bee.Damage(rand.NextDouble() * 80f);

            // Refresh the table to update "Is Dead" column.
            BeeGrid.Items.Refresh();
        }
    }
}
