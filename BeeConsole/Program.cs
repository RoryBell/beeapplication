﻿using System;
using System.Collections.Generic;
using BeeLibrary;

namespace BeeConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Console application for the Bee Library.");

            List<Bee> beeList = new List<Bee>()
            {
                new WorkerBee() { },
                new QueenBee() { },
                new DroneBee() { }
            };

            // Test the damage to the bees by looping through and displaying the status at certain intervals.
            float damage = 10f;
            int rounds = 9;

            for (int i = 1; i <= rounds; i++)
            {
                // Print the number and new health for this round.
                Console.WriteLine($"\nRound {i} - Health = {100 - ((i) * damage)}");

                // Damage each bee and print its status.
                foreach (Bee bee in beeList)
                {
                    bee.Damage(damage);
                    Console.WriteLine($"{bee.ToString()} is " + (bee.IsDead ? "dead" : "alive"));
                }
            }
        }
    }
}
